#include <catch2/catch_test_macros.hpp>
#include "m6502.h"

class StoreRegisterTest {
protected:
  m6502::CPU cpu;
  m6502::Memory mem;

public:
  void TestStoreRegisterAbsolute(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest);
  void TestStoreRegisterAbsoluteX(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest);
  void TestStoreRegisterAbsoluteY(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest);
  void TestStoreRegisterIndirectX(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest);
  void TestStoreRegisterIndirectY(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest);
  void TestStoreRegisterZeroPage(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest);
  void TestStoreRegisterZeroPageX(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest);
  void TestStoreRegisterZeroPageY(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest);
};

void StoreRegisterTest::TestStoreRegisterAbsolute(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest) {
  using namespace m6502;
  s32 expectedCycles = 4;
  Byte expectedValue = 0x52;

  cpu.*registerToTest = expectedValue;

  mem[0xFFFC] = opcode;
  mem[0xFFFD] = 0x21;
  mem[0xFFFE] = 0x25;
  mem[0x2521] = 0x3E; // ensure that the target register does not have the expected value in it

  s32 actualCycles = cpu.Execute(expectedCycles, mem);
  Byte actualValue = mem[0x2521];

  REQUIRE(expectedCycles == actualCycles);
  REQUIRE(expectedValue == actualValue);
}

void StoreRegisterTest::TestStoreRegisterAbsoluteX(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest) {
  using namespace m6502;
  s32 expectedCycles = 4;
  Byte expectedValue = 0x52;

  cpu.X = 7;
  cpu.*registerToTest = expectedValue;

  mem[0xFFFC] = opcode;
  mem[0xFFFD] = 0x21;
  mem[0xFFFE] = 0x25;
  mem[0x2528] = 0x3E; // ensure that the target register does not have the expected value in it

  s32 actualCycles = cpu.Execute(expectedCycles, mem);
  Byte actualValue = mem[0x2528];

  REQUIRE(expectedCycles == actualCycles);
  REQUIRE(expectedValue == actualValue);
}

void StoreRegisterTest::TestStoreRegisterAbsoluteY(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest) {
  using namespace m6502;
  s32 expectedCycles = 4;
  Byte expectedValue = 0x5B;

  cpu.Y = 3;
  cpu.*registerToTest = expectedValue;

  mem[0xFFFC] = opcode;
  mem[0xFFFD] = 0x21;
  mem[0xFFFE] = 0x25;
  mem[0x2524] = 0x3E; // ensure that the target register does not have the expected value in it

  s32 actualCycles = cpu.Execute(expectedCycles, mem);
  Byte actualValue = mem[0x2524];

  REQUIRE(expectedCycles == actualCycles);
  REQUIRE(expectedValue == actualValue);
}

void StoreRegisterTest::TestStoreRegisterIndirectX(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest) {
  using namespace m6502;
  s32 expectedCycles = 6;
  Byte expectedValue = 0x62;

  cpu.X = 10;
  cpu.*registerToTest = expectedValue;

  mem[0xFFFC] = opcode;
  mem[0xFFFD] = 0x21;
  mem[0x002B] = 0xAB;
  mem[0x00AB] = 0x3E; // ensure that the target register does not have the expected value in it

  s32 actualCycles = cpu.Execute(expectedCycles, mem);
  Byte actualValue = mem[0x00AB];

  REQUIRE(expectedCycles == actualCycles);
  REQUIRE(expectedValue == actualValue);
}

void StoreRegisterTest::TestStoreRegisterIndirectY(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest) {
  using namespace m6502;
  s32 expectedCycles = 6;
  Byte expectedValue = 0x52;

  cpu.Y = 2;
  cpu.*registerToTest = expectedValue;

  mem[0xFFFC] = opcode;
  mem[0xFFFD] = 0x9A;
  mem[0x009A] = 0x12;
  mem[0x009B] = 0x57;
  mem[0x5714] = 0x3E; // ensure that the target register does not have the expected value in it

  s32 actualCycles = cpu.Execute(expectedCycles, mem);
  Byte actualValue = mem[0x5714];

  REQUIRE(expectedCycles == actualCycles);
  REQUIRE(expectedValue == actualValue);
}

void StoreRegisterTest::TestStoreRegisterZeroPage(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest) {
  using namespace m6502;
  s32 expectedCycles = 3;
  Byte expectedValue = 0x52;

  cpu.*registerToTest = expectedValue;

  mem[0xFFFC] = opcode;
  mem[0xFFFD] = 0x9A;
  mem[0x009A] = 0x3E; // ensure that the target register does not have the expected value in it

  s32 actualCycles = cpu.Execute(expectedCycles, mem);
  Byte actualValue = mem[0x009A];

  REQUIRE(expectedCycles == actualCycles);
  REQUIRE(expectedValue == actualValue);
}

void StoreRegisterTest::TestStoreRegisterZeroPageX(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest) {
  using namespace m6502;
  s32 expectedCycles = 4;
  Byte expectedValue = 0x52;

  cpu.X = 2;
  cpu.*registerToTest = expectedValue;

  mem[0xFFFC] = opcode;
  mem[0xFFFD] = 0x9A;
  mem[0x009C] = 0x3E; // ensure that the target register does not have the expected value in it

  s32 actualCycles = cpu.Execute(expectedCycles, mem);
  Byte actualValue = mem[0x009C];

  REQUIRE(expectedCycles == actualCycles);
  REQUIRE(expectedValue == actualValue);
}

void StoreRegisterTest::TestStoreRegisterZeroPageY(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest) {
  using namespace m6502;
  s32 expectedCycles = 4;
  Byte expectedValue = 0x72;

  cpu.Y = 9;
  cpu.*registerToTest = expectedValue;

  mem[0xFFFC] = opcode;
  mem[0xFFFD] = 0x12;
  mem[0x001B] = 0x3E; // ensure that the target register does not have the expected value in it

  s32 actualCycles = cpu.Execute(expectedCycles, mem);
  Byte actualValue = mem[0x001B];

  REQUIRE(expectedCycles == actualCycles);
  REQUIRE(expectedValue == actualValue);
}

TEST_CASE_METHOD(StoreRegisterTest, "Store Register A") {
  using namespace m6502;

  cpu.Reset(0xFFFC, mem);

  SECTION("STA Absolute can store the expected value in a specified address") {
    TestStoreRegisterAbsolute(CPU::INS_STA_ABS, &CPU::A);
  }

  SECTION("STA Absolute X can store the expected value in a specified address") {
    TestStoreRegisterAbsoluteX(CPU::INS_STA_ABSX, &CPU::A);
  }
  SECTION("STA Absolute Y can store the expected value in a specified address") {
    TestStoreRegisterAbsoluteY(CPU::INS_STA_ABSY, &CPU::A);
  }
  SECTION("STA Indirect X can store the expected value in a specified address") {
    TestStoreRegisterIndirectX(CPU::INS_STA_INDX, &CPU::A);
  }

  SECTION("STA Indirect Y can store the expected value in a specified address") {
    TestStoreRegisterIndirectY(CPU::INS_STA_INDY, &CPU::A);
  }

  SECTION("STA Zero Page can store the expected value in a specified address") {
    TestStoreRegisterZeroPage(CPU::INS_STA_ZP, &CPU::A);
  }

  SECTION("STA Zero Page X can store the expected value in a specified address") {
    TestStoreRegisterZeroPageX(CPU::INS_STA_ZPX, &CPU::A);
  }
}

TEST_CASE_METHOD(StoreRegisterTest, "Store Register X") {
  using namespace m6502;

  cpu.Reset(0xFFFC, mem);

  SECTION("STX Absolute can store the expected value in a specified address") {
    TestStoreRegisterAbsolute(CPU::INS_STX_ABS, &CPU::X);
  }

  SECTION("STX Zero Page can store the expected value in a specified address") {
    TestStoreRegisterZeroPage(CPU::INS_STX_ZP, &CPU::X);
  }

  SECTION("STX Zero Page Y can store the expected value in a specified address") {
    TestStoreRegisterZeroPageY(CPU::INS_STX_ZPY, &CPU::X);
  }
}

TEST_CASE_METHOD(StoreRegisterTest, "Store Register Y") {
  using namespace m6502;

  cpu.Reset(0xFFFC, mem);

  SECTION("STY Absolute can store the expected value in a specified address") {
    TestStoreRegisterAbsolute(CPU::INS_STY_ABS, &CPU::Y);
  }

  SECTION("STY Zero Page can store the expected value in a specified address") {
    TestStoreRegisterZeroPage(CPU::INS_STY_ZP, &CPU::Y);
  }

  SECTION("STY Zero Page X can store the expected value in a specified address") {
    TestStoreRegisterZeroPageX(CPU::INS_STY_ZPX, &CPU::Y);
  }
}