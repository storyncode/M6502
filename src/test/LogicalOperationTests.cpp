#include <catch2/catch_test_macros.hpp>
#include "m6502.h"

class LogicalOperationTest {
protected:
  m6502::CPU cpu;
  m6502::Memory mem;
public:
  void ExpectNegativeFlagSet(m6502::s32 expectedCycles, m6502::Byte expectedValue);
  void ExpectNoFlagsSet(m6502::s32 expectedCycles, m6502::Byte expectedValue);
  void ExpectOverflowFlagSet(m6502::s32 expectedCycles, m6502::Byte expectedValue);
  void ExpectZeroFlagSet(m6502::s32 expectedCycles, m6502::Byte expectedValue);
  void SetupAbsoluteAddressingTest(m6502::Byte opcode, m6502::Byte comparisonValue);
  void SetupAbsoluteXAddressingTest(m6502::Byte opcode, m6502::Byte comparisonValue);
  void SetupAbsoluteXAddressingWithCrossedPageTest(m6502::Byte opcode, m6502::Byte comparisonValue);
  void SetupAbsoluteYAddressingTest(m6502::Byte opcode, m6502::Byte comparisonValue);
  void SetupAbsoluteYAddressingWithCrossedPageTest(m6502::Byte opcode, m6502::Byte comparisonValue);
  void SetupImmediateAddressingTest(m6502::Byte opcode, m6502::Byte comparisonValue);
  void SetupIndirectXAddressingTest(m6502::Byte opcode, m6502::Byte comparisonValue);
  void SetupIndirectYAddressingTest(m6502::Byte opcode, m6502::Byte comparisonValue);
  void SetupIndirectYAddressingWithCrossedPageTest(m6502::Byte opcode, m6502::Byte comparisonValue);
  void SetupZeroPageAddressingTest(m6502::Byte opcode, m6502::Byte comparisonValue);
  void SetupZeroPageXAddressingTest(m6502::Byte opcode, m6502::Byte comparisonValue);
};

void LogicalOperationTest::ExpectNegativeFlagSet(m6502::s32 expectedCycles, m6502::Byte expectedValue) {
  using namespace m6502;
  s32 actualCycles = cpu.Execute(expectedCycles, mem);
  Byte actualValue = cpu.A;

  REQUIRE(expectedCycles == actualCycles);
  REQUIRE(expectedValue == actualValue);
  REQUIRE(cpu.Flag.N == 1);
  REQUIRE(cpu.Flag.Z == 0);
}

void LogicalOperationTest::ExpectNoFlagsSet(m6502::s32 expectedCycles, m6502::Byte expectedValue) {
  using namespace m6502;
  s32 actualCycles = cpu.Execute(expectedCycles, mem);
  Byte actualValue = cpu.A;

  REQUIRE(expectedCycles == actualCycles);
  REQUIRE(expectedValue == actualValue);
  REQUIRE(cpu.Flag.N == 0);
  REQUIRE(cpu.Flag.Z == 0);
  REQUIRE(cpu.Flag.V == 0);
}

void LogicalOperationTest::ExpectOverflowFlagSet(m6502::s32 expectedCycles, m6502::Byte expectedValue) {
  using namespace m6502;
  s32 actualCycles = cpu.Execute(expectedCycles, mem);
  Byte actualValue = cpu.A;

  REQUIRE(expectedCycles == actualCycles);
  REQUIRE(expectedValue == actualValue);
  REQUIRE(cpu.Flag.N == 0);
  REQUIRE(cpu.Flag.Z == 0);
  REQUIRE(cpu.Flag.V == 1);
}

void LogicalOperationTest::ExpectZeroFlagSet(m6502::s32 expectedCycles, m6502::Byte expectedValue) {
  using namespace m6502;
  s32 actualCycles = cpu.Execute(expectedCycles, mem);
  Byte actualValue = cpu.A;

  REQUIRE(expectedCycles == actualCycles);
  REQUIRE(expectedValue == actualValue);
  REQUIRE(cpu.Flag.N == 0);
  REQUIRE(cpu.Flag.Z == 1);
}

void LogicalOperationTest::SetupAbsoluteAddressingTest(m6502::Byte opcode, m6502::Byte comparisonValue) {
  mem[0xFFFC] = opcode;
  mem[0xFFFD] = 0xF5;
  mem[0xFFFE] = 0x3A;
  mem[0x3AF5] = comparisonValue;
}

void LogicalOperationTest::SetupAbsoluteXAddressingTest(m6502::Byte opcode, m6502::Byte comparisonValue) {
  cpu.X = 0x09;

  // 0b0001 0000 1001 0001 Address                    (0x1091)
  // 0b0001 0000 1001 1010 Address + X                (0x109A)
  // 0b0000 0000 0000 1011 Address ^ (Address + X)    (0x000B)
  // 0b0000 0000 0000 0000 Address >> 8               (0x0000)
  // should not consume additional cycle

  mem[0xFFFC] = opcode;
  mem[0xFFFD] = 0x91;
  mem[0xFFFE] = 0x10;
  mem[0x109A] = comparisonValue;
}

void LogicalOperationTest::SetupAbsoluteXAddressingWithCrossedPageTest(m6502::Byte opcode, m6502::Byte comparisonValue) {
  cpu.X = 0xFF;

  // 0b0001 0000 1001 0001 Address                    (0x1091)
  // 0b0001 0001 1001 0000 Address + X                (0x1190)
  // 0b0000 0001 0000 0001 Address ^ (Address + X)    (0x0100)
  // 0b0000 0000 0000 0000 Address >> 8               (0x0001)
  // should consume additional cycle

  mem[0xFFFC] = opcode;
  mem[0xFFFD] = 0x91;
  mem[0xFFFE] = 0x10;
  mem[0x1190] = comparisonValue;
}

void LogicalOperationTest::SetupAbsoluteYAddressingTest(m6502::Byte opcode, m6502::Byte comparisonValue) {
  cpu.Y = 0x09;

  mem[0xFFFC] = opcode;
  mem[0xFFFD] = 0x91;
  mem[0xFFFE] = 0x10;
  mem[0x109A] = comparisonValue;
}

void LogicalOperationTest::SetupAbsoluteYAddressingWithCrossedPageTest(m6502::Byte opcode, m6502::Byte comparisonValue) {
  cpu.Y = 0xFF;

  mem[0xFFFC] = opcode;
  mem[0xFFFD] = 0x91;
  mem[0xFFFE] = 0x10;
  mem[0x1190] = comparisonValue;
}

void LogicalOperationTest::SetupImmediateAddressingTest(m6502::Byte opcode, m6502::Byte comparisonValue) {
  mem[0xFFFC] = opcode;
  mem[0xFFFD] = comparisonValue;
}

void LogicalOperationTest::SetupIndirectXAddressingTest(m6502::Byte opcode, m6502::Byte comparisonValue) {
  cpu.X = 0x19;

  mem[0xFFFC] = opcode;
  mem[0xFFFD] = 0x91;
  mem[0x00AA] = 0x55;
  mem[0x00AB] = 0xA1;
  mem[0xA155] = comparisonValue;
}

void LogicalOperationTest::SetupIndirectYAddressingTest(m6502::Byte opcode, m6502::Byte comparisonValue) {
  cpu.Y = 0x19;

  mem[0xFFFC] = opcode;
  mem[0xFFFD] = 0x91;
  mem[0x0091] = 0x55;
  mem[0x0092] = 0xA1;
  mem[0xA16E] = comparisonValue;
}

void LogicalOperationTest::SetupIndirectYAddressingWithCrossedPageTest(m6502::Byte opcode, m6502::Byte comparisonValue) {
  cpu.Y = 0xFF;

  mem[0xFFFC] = opcode;
  mem[0xFFFD] = 0x91;
  mem[0x0091] = 0x55;
  mem[0x0092] = 0xA1;
  mem[0xA254] = comparisonValue;
}

void LogicalOperationTest::SetupZeroPageAddressingTest(m6502::Byte opcode, m6502::Byte comparisonValue) {
  mem[0xFFFC] = opcode;
  mem[0xFFFD] = 0x1A;
  mem[0x001A] = comparisonValue;
}

void LogicalOperationTest::SetupZeroPageXAddressingTest(m6502::Byte opcode, m6502::Byte comparisonValue) {
  cpu.X = 0x2F;

  mem[0xFFFC] = opcode;
  mem[0xFFFD] = 0x1A;
  mem[0x0049] = comparisonValue;
}

TEST_CASE_METHOD(LogicalOperationTest, "And operations")
{
  using namespace m6502;
  cpu.Reset(mem);

  SECTION("Absolute addressing should not set any flags when result is not zero and bit 7 is not set") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0x7F; // 0b01111111

    cpu.A = 1;

    Byte expectedValue = comparisonValue & cpu.A; // 0b00000001
    SetupAbsoluteAddressingTest(CPU::INS_AND_ABS, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Absolute addressing should set negative flag when result has bit 7 set") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0xCA;

    cpu.A = 0x9A;

    Byte expectedValue = comparisonValue & cpu.A;
    SetupAbsoluteAddressingTest(CPU::INS_AND_ABS, comparisonValue);
    ExpectNegativeFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Absolute addressing should set zero flag when result is zero") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0;

    cpu.A = 0x9A;

    Byte expectedValue = comparisonValue & cpu.A;
    SetupAbsoluteAddressingTest(CPU::INS_AND_ABS, comparisonValue);
    ExpectZeroFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Absolute X addressing takes an additional cycle when a page boundary is crossed") {
    s32 expectedCycles = 5;
    Byte comparisonValue = 0x7F; // 0b01111111

    cpu.A = 1;

    Byte expectedValue = comparisonValue & cpu.A; // 0b00000001
    SetupAbsoluteXAddressingWithCrossedPageTest(CPU::INS_AND_ABSX, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Absolute X addressing should not set any flags when result is not zero and bit 7 is not set") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0x7F; // 0b01111111

    cpu.A = 1;

    Byte expectedValue = comparisonValue & cpu.A; // 0b00000001
    SetupAbsoluteXAddressingTest(CPU::INS_AND_ABSX, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Absolute X addressing should set negative flag when result has bit 7 set") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0xCA;

    cpu.A = 0x9A;

    Byte expectedValue = comparisonValue & cpu.A;
    SetupAbsoluteXAddressingTest(CPU::INS_AND_ABSX, comparisonValue);
    ExpectNegativeFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Absolute X addressing should set zero flag when result is zero") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0;

    cpu.A = 0x9A;

    Byte expectedValue = comparisonValue & cpu.A;
    SetupAbsoluteXAddressingTest(CPU::INS_AND_ABSX, comparisonValue);
    ExpectZeroFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Absolute Y addressing should take an additional cycle when a page boundary is crossed") {
    s32 expectedCycles = 5;
    Byte comparisonValue = 0x7F; // 0b01111111

    cpu.A = 1;

    Byte expectedValue = comparisonValue & cpu.A; // 0b00000001
    SetupAbsoluteYAddressingWithCrossedPageTest(CPU::INS_AND_ABSY, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Absolute Y addressing should not set any flags when result is not zero and bit 7 is not set") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0x7F; // 0b01111111

    cpu.A = 1;

    Byte expectedValue = comparisonValue & cpu.A; // 0b00000001
    SetupAbsoluteYAddressingTest(CPU::INS_AND_ABSY, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Absolute Y addressing should set negative flag when result has bit 7 set") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0xCA;

    cpu.A = 0x9A;

    Byte expectedValue = comparisonValue & cpu.A;
    SetupAbsoluteYAddressingTest(CPU::INS_AND_ABSY, comparisonValue);
    ExpectNegativeFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Absolute Y addressing should set zero flag when result is zero") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0;

    cpu.A = 0x9A;

    Byte expectedValue = comparisonValue & cpu.A;
    SetupAbsoluteYAddressingTest(CPU::INS_AND_ABSY, comparisonValue);
    ExpectZeroFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Immediate addressing should not set any flags when result is not zero and bit 7 is not set") {
    s32 expectedCycles = 2;
    Byte comparisonValue = 0x7F; // 0b01111111

    cpu.A = 1;

    Byte expectedValue = comparisonValue & cpu.A; // 0b00000001
    SetupImmediateAddressingTest(CPU::INS_AND_IM, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Immediate addressing should set negative flag when result has bit 7 set") {
    s32 expectedCycles = 2;
    Byte comparisonValue = 0xCA;

    cpu.A = 0x9A;

    Byte expectedValue = comparisonValue & cpu.A;
    SetupImmediateAddressingTest(CPU::INS_AND_IM, comparisonValue);
    ExpectNegativeFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Immediate addressing should set zero flag when result is zero") {
    s32 expectedCycles = 2;
    Byte comparisonValue = 0;

    cpu.A = 0x9A;

    Byte expectedValue = comparisonValue & cpu.A;
    SetupImmediateAddressingTest(CPU::INS_AND_IM, comparisonValue);
    ExpectZeroFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Indirect X addressing should not set any flags when result is not zero and bit 7 is not set") {
    s32 expectedCycles = 6;
    Byte comparisonValue = 0x7F; // 0b01111111

    cpu.A = 1;

    Byte expectedValue = comparisonValue & cpu.A; // 0b00000001
    SetupIndirectXAddressingTest(CPU::INS_AND_INDX, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Indirect X addressing should set negative flag when result has bit 7 set") {
    s32 expectedCycles = 6;
    Byte comparisonValue = 0xCA;

    cpu.A = 0x9A;

    Byte expectedValue = comparisonValue & cpu.A;
    SetupIndirectXAddressingTest(CPU::INS_AND_INDX, comparisonValue);
    ExpectNegativeFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Indirect X addressing should set zero flag when result is zero") {
    s32 expectedCycles = 6;
    Byte comparisonValue = 0;

    cpu.A = 0x9A;

    Byte expectedValue = comparisonValue & cpu.A;
    SetupIndirectXAddressingTest(CPU::INS_AND_INDX, comparisonValue);
    ExpectZeroFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Indirect Y addressing should take an additional cycle when a page boundary is crossed") {
    s32 expectedCycles = 6;
    Byte comparisonValue = 0x01;

    cpu.A = 1;

    Byte expectedValue = comparisonValue & cpu.A; // 0b00000001
    SetupIndirectYAddressingWithCrossedPageTest(CPU::INS_AND_INDY, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Indirect Y addressing should not set any flags when result is not zero and bit 7 is not set") {
    s32 expectedCycles = 5;
    Byte comparisonValue = 0x7F; // 0b01111111

    cpu.A = 1;

    Byte expectedValue = comparisonValue & cpu.A; // 0b00000001
    SetupIndirectYAddressingTest(CPU::INS_AND_INDY, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Indirect Y addressing should set negative flag when result has bit 7 set") {
    s32 expectedCycles = 5;
    Byte comparisonValue = 0xCA;

    cpu.A = 0x9A;

    Byte expectedValue = comparisonValue & cpu.A;
    SetupIndirectYAddressingTest(CPU::INS_AND_INDY, comparisonValue);
    ExpectNegativeFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Indirect Y addressing should set zero flag when result is zero") {
    s32 expectedCycles = 5;
    Byte comparisonValue = 0;

    cpu.A = 0x9A;

    Byte expectedValue = comparisonValue & cpu.A;
    SetupIndirectYAddressingTest(CPU::INS_AND_INDY, comparisonValue);
    ExpectZeroFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Zero Page addressing should not set any flags when result is not zero and bit 7 is not set") {
    s32 expectedCycles = 3;
    Byte comparisonValue = 0x7F; // 0b01111111

    cpu.A = 1;

    Byte expectedValue = comparisonValue & cpu.A; // 0b00000001
    SetupZeroPageAddressingTest(CPU::INS_AND_ZP, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Zero Page addressing should set negative flag when result has bit 7 set") {
    s32 expectedCycles = 3;
    Byte comparisonValue = 0xCA;

    cpu.A = 0x9A;

    Byte expectedValue = comparisonValue & cpu.A;
    SetupZeroPageAddressingTest(CPU::INS_AND_ZP, comparisonValue);
    ExpectNegativeFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Zero Page addressing should set zero flag when result is zero") {
    s32 expectedCycles = 3;
    Byte comparisonValue = 0;

    cpu.A = 0x9A;

    Byte expectedValue = comparisonValue & cpu.A;
    SetupZeroPageAddressingTest(CPU::INS_AND_ZP, comparisonValue);
    ExpectZeroFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Zero Page X addressing should not set any flags when result is not zero and bit 7 is not set") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0x7F; // 0b01111111

    cpu.A = 1;

    Byte expectedValue = comparisonValue & cpu.A; // 0b00000001
    SetupZeroPageXAddressingTest(CPU::INS_AND_ZPX, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Zero Page X addressing should set negative flag when result has bit 7 set") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0xCA;

    cpu.A = 0x9A;

    Byte expectedValue = comparisonValue & cpu.A;
    SetupZeroPageXAddressingTest(CPU::INS_AND_ZPX, comparisonValue);
    ExpectNegativeFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Zero Page X addressing should set zero flag when result is zero") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0;

    cpu.A = 0x9A;

    Byte expectedValue = comparisonValue & cpu.A;
    SetupZeroPageXAddressingTest(CPU::INS_AND_ZPX, comparisonValue);
    ExpectZeroFlagSet(expectedCycles, expectedValue);
  }
}

TEST_CASE_METHOD(LogicalOperationTest, "Bit operations") {
  using namespace m6502;
  cpu.Reset(mem);

  SECTION("Absolute addressing should not set any flags when result is not zero and bit 6 & 7 are not set") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0x01;

    cpu.A = 0xFF;

    Byte expectedValue = cpu.A; // 0b00000001
    SetupAbsoluteAddressingTest(CPU::INS_BIT_ABS, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Absolute addressing should set negative flag when result has bit 7 set") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0xCA;

    cpu.A = 0x9A;

    Byte expectedValue = cpu.A;
    SetupAbsoluteAddressingTest(CPU::INS_BIT_ABS, comparisonValue);
    ExpectNegativeFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Absolute addressing should set overflow flag when result has bit 6 set") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0x72;

    cpu.A = 0xA2;

    Byte expectedValue = cpu.A;
    SetupAbsoluteAddressingTest(CPU::INS_BIT_ABS, comparisonValue);
    ExpectOverflowFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Absolute addressing should set zero flag when result is zero") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0;

    cpu.A = 0x9A;

    Byte expectedValue = cpu.A;
    SetupAbsoluteAddressingTest(CPU::INS_BIT_ABS, comparisonValue);
    ExpectZeroFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Zero Page addressing should not set any flags when result is not zero and bit 7 is not set") {
    s32 expectedCycles = 3;
    Byte comparisonValue = 0x01; // 0b01111111

    cpu.A = 0xFF;

    Byte expectedValue = cpu.A; // 0b00000001
    SetupZeroPageAddressingTest(CPU::INS_BIT_ZP, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Zero Page addressing should set negative flag when result has bit 7 set") {
    s32 expectedCycles = 3;
    Byte comparisonValue = 0xCA;

    cpu.A = 0xCA;

    Byte expectedValue = cpu.A;
    SetupZeroPageAddressingTest(CPU::INS_BIT_ZP, comparisonValue);
    ExpectNegativeFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Zero Page addressing should set overflow flag when result has bit 6 set") {
    s32 expectedCycles = 3;
    Byte comparisonValue = 0x70;

    cpu.A = 0xCF;

    Byte expectedValue = cpu.A;
    SetupZeroPageAddressingTest(CPU::INS_BIT_ZP, comparisonValue);
    ExpectOverflowFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Zero Page addressing should set zero flag when result is zero") {
    s32 expectedCycles = 3;
    Byte comparisonValue = 0;

    cpu.A = 0x9A;

    Byte expectedValue = cpu.A;
    SetupZeroPageAddressingTest(CPU::INS_BIT_ZP, comparisonValue);
    ExpectZeroFlagSet(expectedCycles, expectedValue);
  }
}

TEST_CASE_METHOD(LogicalOperationTest, "Exclusive Or operations")
{
  using namespace m6502;
  cpu.Reset(mem);

  SECTION("Absolute addressing should not set any flags when result is not zero and bit 7 is not set") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0x7F; // 0b01111111

    cpu.A = 1;

    Byte expectedValue = comparisonValue ^ cpu.A; // 0b00000001
    SetupAbsoluteAddressingTest(CPU::INS_EOR_ABS, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Absolute addressing should set negative flag when result has bit 7 set") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0xCA;

    cpu.A = 0x7A;

    Byte expectedValue = comparisonValue ^ cpu.A;
    SetupAbsoluteAddressingTest(CPU::INS_EOR_ABS, comparisonValue);
    ExpectNegativeFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Absolute addressing should set zero flag when result is zero") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0xFF;

    cpu.A = 0xFF;

    Byte expectedValue = comparisonValue ^ cpu.A;
    SetupAbsoluteAddressingTest(CPU::INS_EOR_ABS, comparisonValue);
    ExpectZeroFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Absolute X addressing should consume an additional cycle when a page boundary is crossed") {
    s32 expectedCycles = 5;
    Byte comparisonValue = 0x7F; // 0b01111111

    cpu.A = 0b01000000;

    Byte expectedValue = comparisonValue ^ cpu.A; // 0b00000001
    SetupAbsoluteXAddressingWithCrossedPageTest(CPU::INS_EOR_ABSX, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Absolute X addressing should not set any flags when result is not zero and bit 7 is not set") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0x7F; // 0b01111111

    cpu.A = 0b01000000;

    Byte expectedValue = comparisonValue ^ cpu.A; // 0b00000001
    SetupAbsoluteXAddressingTest(CPU::INS_EOR_ABSX, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Absolute X addressing should set negative flag when result has bit 7 set") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0xCA;

    cpu.A = 0x7A;

    Byte expectedValue = comparisonValue ^ cpu.A;
    SetupAbsoluteXAddressingTest(CPU::INS_EOR_ABSX, comparisonValue);
    ExpectNegativeFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Absolute X addressing should set zero flag when result is zero") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0;

    cpu.A = 0;

    Byte expectedValue = comparisonValue ^ cpu.A;
    SetupAbsoluteXAddressingTest(CPU::INS_EOR_ABSX, comparisonValue);
    ExpectZeroFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Absolute Y addressing should consume an additional cycle when a page boundary is crossed") {
    s32 expectedCycles = 5;
    Byte comparisonValue = 0x7F; // 0b01111111

    cpu.A = 1;

    Byte expectedValue = comparisonValue ^ cpu.A; // 0b00000001
    SetupAbsoluteYAddressingWithCrossedPageTest(CPU::INS_EOR_ABSY, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Absolute Y addressing should not set any flags when result is not zero and bit 7 is not set") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0x7F; // 0b01111111

    cpu.A = 1;

    Byte expectedValue = comparisonValue ^ cpu.A; // 0b00000001
    SetupAbsoluteYAddressingTest(CPU::INS_EOR_ABSY, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Absolute Y addressing should set negative flag when result has bit 7 set") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0xCA;

    cpu.A = 0x7A;

    Byte expectedValue = comparisonValue ^ cpu.A;
    SetupAbsoluteYAddressingTest(CPU::INS_EOR_ABSY, comparisonValue);
    ExpectNegativeFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Absolute Y addressing should set zero flag when result is zero") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0x7A;

    cpu.A = 0x7A;

    Byte expectedValue = comparisonValue ^ cpu.A;
    SetupAbsoluteYAddressingTest(CPU::INS_EOR_ABSY, comparisonValue);
    ExpectZeroFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Immediate addressing should not set any flags when result is not zero and bit 7 is not set") {
    s32 expectedCycles = 2;
    Byte comparisonValue = 0x7F; // 0b01111111

    cpu.A = 1;

    Byte expectedValue = comparisonValue ^ cpu.A; // 0b00000001
    SetupImmediateAddressingTest(CPU::INS_EOR_IM, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Immediate addressing should set negative flag when result has bit 7 set") {
    s32 expectedCycles = 2;
    Byte comparisonValue = 0xCA;

    cpu.A = 0x7A;

    Byte expectedValue = comparisonValue ^ cpu.A;
    SetupImmediateAddressingTest(CPU::INS_EOR_IM, comparisonValue);
    ExpectNegativeFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Immediate addressing should set zero flag when result is zero") {
    s32 expectedCycles = 2;
    Byte comparisonValue = 0;

    cpu.A = 0;

    Byte expectedValue = comparisonValue ^ cpu.A;
    SetupImmediateAddressingTest(CPU::INS_EOR_IM, comparisonValue);
    ExpectZeroFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Indirect X addressing should not set any flags when result is not zero and bit 7 is not set") {
    s32 expectedCycles = 6;
    Byte comparisonValue = 0x7F; // 0b01111111

    cpu.A = 1;

    Byte expectedValue = comparisonValue ^ cpu.A; // 0b00000001
    SetupIndirectXAddressingTest(CPU::INS_EOR_INDX, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Indirect X addressing should set negative flag when result has bit 7 set") {
    s32 expectedCycles = 6;
    Byte comparisonValue = 0xCA;

    cpu.A = 0x7A;

    Byte expectedValue = comparisonValue ^ cpu.A;
    SetupIndirectXAddressingTest(CPU::INS_EOR_INDX, comparisonValue);
    ExpectNegativeFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Indirect X addressing should set zero flag when result is zero") {
    s32 expectedCycles = 6;
    Byte comparisonValue = 0;

    cpu.A = 0;

    Byte expectedValue = comparisonValue ^ cpu.A;
    SetupIndirectXAddressingTest(CPU::INS_EOR_INDX, comparisonValue);
    ExpectZeroFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Indirect Y addressing should consume an additional cycle if a page boundary is crossed") {
    s32 expectedCycles = 6;
    Byte comparisonValue = 0x7F; // 0b01111111

    cpu.A = 1;

    Byte expectedValue = comparisonValue ^ cpu.A; // 0b00000001
    SetupIndirectYAddressingWithCrossedPageTest(CPU::INS_EOR_INDY, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Indirect Y addressing should not set any flags when result is not zero and bit 7 is not set") {
    s32 expectedCycles = 5;
    Byte comparisonValue = 0x7F; // 0b01111111

    cpu.A = 1;

    Byte expectedValue = comparisonValue ^ cpu.A; // 0b00000001
    SetupIndirectYAddressingTest(CPU::INS_EOR_INDY, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Indirect Y addressing should set negative flag when result has bit 7 set") {
    s32 expectedCycles = 5;
    Byte comparisonValue = 0xCA;

    cpu.A = 0x7A;

    Byte expectedValue = comparisonValue ^ cpu.A;
    SetupIndirectYAddressingTest(CPU::INS_EOR_INDY, comparisonValue);
    ExpectNegativeFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Indirect Y addressing should set zero flag when result is zero") {
    s32 expectedCycles = 5;
    Byte comparisonValue = 0;

    cpu.A = 0;

    Byte expectedValue = comparisonValue ^ cpu.A;
    SetupIndirectYAddressingTest(CPU::INS_EOR_INDY, comparisonValue);
    ExpectZeroFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Zero Page addressing should not set any flags when result is not zero and bit 7 is not set") {
    s32 expectedCycles = 3;
    Byte comparisonValue = 0x7F; // 0b01111111

    cpu.A = 1;

    Byte expectedValue = comparisonValue ^ cpu.A; // 0b00000001
    SetupZeroPageAddressingTest(CPU::INS_EOR_ZP, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Zero Page addressing should set negative flag when result has bit 7 set") {
    s32 expectedCycles = 3;
    Byte comparisonValue = 0xCA; // 0b11001010

    cpu.A = 0x7A; // 0b01001010

    Byte expectedValue = comparisonValue ^ cpu.A; // 0b10000000
    SetupZeroPageAddressingTest(CPU::INS_EOR_ZP, comparisonValue);
    ExpectNegativeFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Zero Page addressing should set zero flag when result is zero") {
    s32 expectedCycles = 3;
    Byte comparisonValue = 0;

    cpu.A = 0;

    Byte expectedValue = comparisonValue ^ cpu.A;
    SetupZeroPageAddressingTest(CPU::INS_EOR_ZP, comparisonValue);
    ExpectZeroFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Zero Page X addressing should not set any flags when result is not zero and bit 7 is not set") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0x7F; // 0b01111111

    cpu.A = 1;

    Byte expectedValue = comparisonValue ^ cpu.A; // 0b00000001
    SetupZeroPageXAddressingTest(CPU::INS_EOR_ZPX, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Zero Page X addressing should set negative flag when result has bit 7 set") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0xCA;

    cpu.A = 0x7A;

    Byte expectedValue = comparisonValue ^ cpu.A;
    SetupZeroPageXAddressingTest(CPU::INS_EOR_ZPX, comparisonValue);
    ExpectNegativeFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Zero Page X addressing should set zero flag when result is zero") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0;

    cpu.A = 0;

    Byte expectedValue = comparisonValue ^ cpu.A;
    SetupZeroPageXAddressingTest(CPU::INS_EOR_ZPX, comparisonValue);
    ExpectZeroFlagSet(expectedCycles, expectedValue);
  }
}

TEST_CASE_METHOD(LogicalOperationTest, "Or operations")
{
  using namespace m6502;
  cpu.Reset(mem);

  SECTION("Absolute addressing should not set any flags when result is not zero and bit 7 is not set") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0x7F; // 0b01111111

    cpu.A = 1;

    Byte expectedValue = comparisonValue | cpu.A; // 0b00000001
    SetupAbsoluteAddressingTest(CPU::INS_ORA_ABS, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Absolute addressing should set negative flag when result has bit 7 set") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0xCA;

    cpu.A = 0x9A;

    Byte expectedValue = comparisonValue | cpu.A;
    SetupAbsoluteAddressingTest(CPU::INS_ORA_ABS, comparisonValue);
    ExpectNegativeFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Absolute addressing should set zero flag when result is zero") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0;

    cpu.A = 0;

    Byte expectedValue = comparisonValue | cpu.A;
    SetupAbsoluteAddressingTest(CPU::INS_ORA_ABS, comparisonValue);
    ExpectZeroFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Absolute X addressing should not set any flags when result is not zero and bit 7 is not set") {
    s32 expectedCycles = 5;
    Byte comparisonValue = 0x7F; // 0b01111111

    cpu.A = 1;

    Byte expectedValue = comparisonValue | cpu.A; // 0b00000001
    SetupAbsoluteXAddressingWithCrossedPageTest(CPU::INS_ORA_ABSX, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Absolute X addressing should not set any flags when result is not zero and bit 7 is not set") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0x7F; // 0b01111111

    cpu.A = 1;

    Byte expectedValue = comparisonValue | cpu.A; // 0b00000001
    SetupAbsoluteXAddressingTest(CPU::INS_ORA_ABSX, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Absolute X addressing should set negative flag when result has bit 7 set") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0xCA;

    cpu.A = 0x9A;

    Byte expectedValue = comparisonValue | cpu.A;
    SetupAbsoluteXAddressingTest(CPU::INS_ORA_ABSX, comparisonValue);
    ExpectNegativeFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Absolute X addressing should set zero flag when result is zero") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0;

    cpu.A = 0;

    Byte expectedValue = comparisonValue | cpu.A;
    SetupAbsoluteXAddressingTest(CPU::INS_ORA_ABSX, comparisonValue);
    ExpectZeroFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Absolute Y addressing should consume an extra cycle when page boundary is crossed") {
    s32 expectedCycles = 5;
    Byte comparisonValue = 0x7F; // 0b01111111

    cpu.A = 1;

    Byte expectedValue = comparisonValue | cpu.A;
    SetupAbsoluteYAddressingWithCrossedPageTest(CPU::INS_ORA_ABSY, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Absolute Y addressing should not set any flags when result is not zero and bit 7 is not set") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0x7F; // 0b01111111

    cpu.A = 1;

    Byte expectedValue = comparisonValue | cpu.A; // 0b00000001
    SetupAbsoluteYAddressingTest(CPU::INS_ORA_ABSY, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Absolute Y addressing should set negative flag when result has bit 7 set") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0xCA;

    cpu.A = 0x9A;

    Byte expectedValue = comparisonValue | cpu.A;
    SetupAbsoluteYAddressingTest(CPU::INS_ORA_ABSY, comparisonValue);
    ExpectNegativeFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Absolute Y addressing should set zero flag when result is zero") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0;

    cpu.A = 0;

    Byte expectedValue = comparisonValue | cpu.A;
    SetupAbsoluteYAddressingTest(CPU::INS_ORA_ABSY, comparisonValue);
    ExpectZeroFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Immediate addressing should not set any flags when result is not zero and bit 7 is not set") {
    s32 expectedCycles = 2;
    Byte comparisonValue = 0x7F; // 0b01111111

    cpu.A = 1;

    Byte expectedValue = comparisonValue | cpu.A; // 0b00000001
    SetupImmediateAddressingTest(CPU::INS_ORA_IM, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Immediate addressing should set negative flag when result has bit 7 set") {
    s32 expectedCycles = 2;
    Byte comparisonValue = 0xCA;

    cpu.A = 0x9A;

    Byte expectedValue = comparisonValue | cpu.A;
    SetupImmediateAddressingTest(CPU::INS_ORA_IM, comparisonValue);
    ExpectNegativeFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Immediate addressing should set zero flag when result is zero") {
    s32 expectedCycles = 2;
    Byte comparisonValue = 0;

    cpu.A = 0;

    Byte expectedValue = comparisonValue | cpu.A;
    SetupImmediateAddressingTest(CPU::INS_ORA_IM, comparisonValue);
    ExpectZeroFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Indirect X addressing should not set any flags when result is not zero and bit 7 is not set") {
    s32 expectedCycles = 6;
    Byte comparisonValue = 0x7F; // 0b01111111

    cpu.A = 1;

    Byte expectedValue = comparisonValue | cpu.A; // 0b00000001
    SetupIndirectXAddressingTest(CPU::INS_ORA_INDX, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Indirect X addressing should set negative flag when result has bit 7 set") {
    s32 expectedCycles = 6;
    Byte comparisonValue = 0xCA;

    cpu.A = 0x9A;

    Byte expectedValue = comparisonValue | cpu.A;
    SetupIndirectXAddressingTest(CPU::INS_ORA_INDX, comparisonValue);
    ExpectNegativeFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Indirect X addressing should set zero flag when result is zero") {
    s32 expectedCycles = 6;
    Byte comparisonValue = 0;

    cpu.A = 0;

    Byte expectedValue = comparisonValue | cpu.A;
    SetupIndirectXAddressingTest(CPU::INS_ORA_INDX, comparisonValue);
    ExpectZeroFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Indirect Y addressing should consume an additional cycle when page boundary is crossed") {
    s32 expectedCycles = 6;
    Byte comparisonValue = 0x7F; // 0b01111111

    cpu.A = 1;

    Byte expectedValue = comparisonValue | cpu.A; // 0b00000001
    SetupIndirectYAddressingWithCrossedPageTest(CPU::INS_ORA_INDY, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Indirect Y addressing should not set any flags when result is not zero and bit 7 is not set") {
    s32 expectedCycles = 5;
    Byte comparisonValue = 0x7F; // 0b01111111

    cpu.A = 1;

    Byte expectedValue = comparisonValue | cpu.A; // 0b00000001
    SetupIndirectYAddressingTest(CPU::INS_ORA_INDY, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Indirect Y addressing should set negative flag when result has bit 7 set") {
    s32 expectedCycles = 5;
    Byte comparisonValue = 0xCA;

    cpu.A = 0x9A;

    Byte expectedValue = comparisonValue | cpu.A;
    SetupIndirectYAddressingTest(CPU::INS_ORA_INDY, comparisonValue);
    ExpectNegativeFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Indirect Y addressing should set zero flag when result is zero") {
    s32 expectedCycles = 5;
    Byte comparisonValue = 0;

    cpu.A = 0;

    Byte expectedValue = comparisonValue | cpu.A;
    SetupIndirectYAddressingTest(CPU::INS_ORA_INDY, comparisonValue);
    ExpectZeroFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Zero Page addressing should not set any flags when result is not zero and bit 7 is not set") {
    s32 expectedCycles = 3;
    Byte comparisonValue = 0x7F; // 0b01111111

    cpu.A = 1;

    Byte expectedValue = comparisonValue | cpu.A; // 0b00000001
    SetupZeroPageAddressingTest(CPU::INS_ORA_ZP, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Zero Page addressing should set negative flag when result has bit 7 set") {
    s32 expectedCycles = 3;
    Byte comparisonValue = 0xCA;

    cpu.A = 0x9A;

    Byte expectedValue = comparisonValue | cpu.A;
    SetupZeroPageAddressingTest(CPU::INS_ORA_ZP, comparisonValue);
    ExpectNegativeFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Zero Page addressing should set zero flag when result is zero") {
    s32 expectedCycles = 3;
    Byte comparisonValue = 0;

    cpu.A = 0;

    Byte expectedValue = comparisonValue | cpu.A;
    SetupZeroPageAddressingTest(CPU::INS_ORA_ZP, comparisonValue);
    ExpectZeroFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Zero Page X addressing should not set any flags when result is not zero and bit 7 is not set") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0x7F; // 0b01111111

    cpu.A = 1;

    Byte expectedValue = comparisonValue | cpu.A;
    SetupZeroPageXAddressingTest(CPU::INS_ORA_ZPX, comparisonValue);
    ExpectNoFlagsSet(expectedCycles, expectedValue);
  }

  SECTION("Zero Page X addressing should set negative flag when result has bit 7 set") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0xCA;

    cpu.A = 0x9A;

    Byte expectedValue = comparisonValue | cpu.A;
    SetupZeroPageXAddressingTest(CPU::INS_ORA_ZPX, comparisonValue);
    ExpectNegativeFlagSet(expectedCycles, expectedValue);
  }

  SECTION("Zero Page X addressing should set zero flag when result is zero") {
    s32 expectedCycles = 4;
    Byte comparisonValue = 0;

    cpu.A = 0;

    Byte expectedValue = comparisonValue | cpu.A;
    SetupZeroPageXAddressingTest(CPU::INS_ORA_ZPX, comparisonValue);
    ExpectZeroFlagSet(expectedCycles, expectedValue);
  }
}