#include <catch2/catch_test_macros.hpp>
#include "m6502.h"

class LoadRegisterTest {
protected:
  m6502::CPU cpu;
  m6502::Memory mem;

public:
  void TestLoadRegisterAbsolute(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest);
  void TestLoadRegisterAbsoluteX(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest);
  void TestLoadRegisterAbsoluteY(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest);
  void TestLoadRegisterAbsoluteYWhenCrossingPage(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest);
  void TestLoadRegisterAbsoluteXWhenCrossingPage(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest);
  void TestLoadRegisterEffectsNegativeFlag(m6502::Byte opcode);
  void TestLoadRegisterEffectsZeroFlag(m6502::Byte opcode);
  void TestLoadRegisterImmediate(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest);
  void TestLoadRegisterIndirectX(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest);
  void TestLoadRegisterIndirectY(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest);
  void TestLoadRegisterZeroPage(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest);
  void TestLoadRegisterZeroPageX(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest);
  void TestLoadRegisterZeroPageY(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest);
  void ValidateUnchangedFlags(m6502::StatusFlags expected, m6502::StatusFlags actual);
};

void LoadRegisterTest::ValidateUnchangedFlags(m6502::StatusFlags expected, m6502::StatusFlags actual) {
  REQUIRE(expected.B == actual.B);
  REQUIRE(expected.C == actual.C);
  REQUIRE(expected.D == actual.D);
  REQUIRE(expected.I == actual.I);
  REQUIRE(expected.V == actual.V);
}

void LoadRegisterTest::TestLoadRegisterAbsolute(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest) {
  using namespace m6502;
  s32 expectedCycles = 4;
  Byte expectedValue = 0x37;

  // setup memory for test
  mem[0xFFFC] = opcode;
  mem[0xFFFD] = 0x42;
  mem[0xFFFE] = 0x42;
  mem[0x4242] = expectedValue;

  // when:
  s32 actualCycles = cpu.Execute(expectedCycles, mem);

  // then:
  REQUIRE(expectedCycles == actualCycles);
  REQUIRE(cpu.*registerToTest == expectedValue);
}

void LoadRegisterTest::TestLoadRegisterAbsoluteX(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest) {
  using namespace m6502;
  s32 expectedCycles = 4;
  Byte expectedValue = 0x37;

  cpu.X = 7;

  // setup memory for test
  mem[0xFFFC] = opcode;
  mem[0xFFFD] = 0x42;
  mem[0xFFFE] = 0x42;
  mem[0x4249] = expectedValue;

  // when:
  s32 actualCycles = cpu.Execute(expectedCycles, mem);

  // then:
  REQUIRE(expectedCycles == actualCycles);
  REQUIRE(cpu.*registerToTest == expectedValue);
}

void LoadRegisterTest::TestLoadRegisterAbsoluteY(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest) {
  using namespace m6502;
  s32 expectedCycles = 4;
  Byte expectedValue = 0x37;

  cpu.Y = 12;

  // setup memory for test
  mem[0xFFFC] = opcode;
  mem[0xFFFD] = 0x42;
  mem[0xFFFE] = 0x42;
  mem[0x424E] = expectedValue;

  // when:
  s32 actualCycles = cpu.Execute(expectedCycles, mem);

  // then:
  REQUIRE(expectedCycles == actualCycles);
  REQUIRE(cpu.*registerToTest == expectedValue);
}
void LoadRegisterTest::TestLoadRegisterAbsoluteXWhenCrossingPage(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest) {
  using namespace m6502;
  s32 expectedCycles = 5;
  Byte expectedValue = 0x37;

  cpu.X = 0xFF;

  // setup memory for test
  mem[0xFFFC] = opcode;
  mem[0xFFFD] = 0x42;
  mem[0xFFFE] = 0x42;
  mem[0x4341] = expectedValue;

  // when:
  s32 actualCycles = cpu.Execute(expectedCycles, mem);

  // then:
  REQUIRE(expectedCycles == actualCycles);
  REQUIRE(cpu.*registerToTest == expectedValue);
}

void LoadRegisterTest::TestLoadRegisterAbsoluteYWhenCrossingPage(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest) {
  using namespace m6502;
  s32 expectedCycles = 5;
  Byte expectedValue = 0x37;

  cpu.Y = 0xFF;

  // setup memory for test
  mem[0xFFFC] = opcode;
  mem[0xFFFD] = 0x42;
  mem[0xFFFE] = 0x42;
  mem[0x4341] = expectedValue;

  // when:
  s32 actualCycles = cpu.Execute(expectedCycles, mem);

  // then:
  REQUIRE(expectedCycles == actualCycles);
  REQUIRE(cpu.*registerToTest == expectedValue);
}

void LoadRegisterTest::TestLoadRegisterImmediate(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest) {
  using namespace m6502;
  s32 expectedCycles = 2;
  Byte expectedValue = 0x37;

  // setup memory for test
  mem[0xFFFC] = opcode;
  mem[0xFFFD] = expectedValue;

  // when:
  s32 actualCycles = cpu.Execute(expectedCycles, mem);

  // then:
  REQUIRE(expectedCycles == actualCycles);
  REQUIRE(cpu.*registerToTest == expectedValue);
}

void LoadRegisterTest::TestLoadRegisterIndirectX(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest) {
  using namespace m6502;
  s32 expectedCycles = 6;
  Byte expectedValue = 0x37;

  cpu.X = 0x20;

  // setup memory for test
  mem[0xFFFC] = opcode;
  mem[0xFFFD] = 0x42;
  mem[0x0062] = 0x98;
  mem[0x0063] = 0x12;
  mem[0x1298] = expectedValue;

  // when:
  s32 actualCycles = cpu.Execute(expectedCycles, mem);

  // then:
  REQUIRE(expectedCycles == actualCycles);
  REQUIRE(cpu.*registerToTest == expectedValue);
}

void LoadRegisterTest::TestLoadRegisterIndirectY(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest) {
  using namespace m6502;
  s32 expectedCycles = 5;
  Byte expectedValue = 0x37;

  cpu.Y = 0x04;
  mem[0xFFFC] = opcode;
  mem[0xFFFD] = 0x02;
  mem[0x0002] = 0x00;
  mem[0x0003] = 0x80;
  mem[0x8004] = expectedValue;

  //when:
  s32 actualCycles = cpu.Execute( expectedCycles, mem );

  //then:
  REQUIRE(cpu.*registerToTest == expectedValue);
  REQUIRE(expectedCycles == actualCycles);
}

void LoadRegisterTest::TestLoadRegisterZeroPage(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest) {
  using namespace m6502;
  s32 expectedCycles = 3;
  Byte expectedValue = 0x37;

  // setup memory for test
  mem[0xFFFC] = opcode;
  mem[0xFFFD] = 0x59;
  mem[0x0059] = expectedValue;

  // when:
  s32 actualCycles = cpu.Execute(expectedCycles, mem);

  // then:
  REQUIRE(expectedCycles == actualCycles);
  REQUIRE(cpu.*registerToTest == expectedValue);
}

void LoadRegisterTest::TestLoadRegisterZeroPageX(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest) {
  using namespace m6502;
  s32 expectedCycles = 4;
  Byte expectedValue = 0x37;

  cpu.X = 5;

  // setup memory for test
  mem[0xFFFC] = opcode;
  mem[0xFFFD] = 0x59;
  mem[0x005E] = expectedValue;

  // when:
  s32 actualCycles = cpu.Execute(expectedCycles, mem);

  // then:
  REQUIRE(expectedCycles == actualCycles);
  REQUIRE(cpu.*registerToTest == expectedValue);
}

void LoadRegisterTest::TestLoadRegisterZeroPageY(m6502::Byte opcode, m6502::Byte m6502::CPU::*registerToTest) {
  using namespace m6502;
  s32 expectedCycles = 4;
  Byte expectedValue = 0x37;

  cpu.Y = 5;

  // setup memory for test
  mem[0xFFFC] = opcode;
  mem[0xFFFD] = 0x75;
  mem[0x007A] = expectedValue;

  // when:
  s32 actualCycles = cpu.Execute(expectedCycles, mem);

  // then:
  REQUIRE(expectedCycles == actualCycles);
  REQUIRE(cpu.*registerToTest == expectedValue);
}

void LoadRegisterTest::TestLoadRegisterEffectsNegativeFlag(m6502::Byte opcode) {
  using namespace m6502;
  CPU cpuCpy;
  s32 expectedCycles = 3;
  Byte expectedValue = 0x96;

  // setup memory for test
  mem[0xFFFC] = opcode;
  mem[0xFFFD] = expectedValue;

  cpuCpy = cpu;

  // when:
  cpu.Execute(expectedCycles, mem);

  // then:
  REQUIRE(cpu.Flag.N == 1);
  REQUIRE(cpu.Flag.Z == 0);
  ValidateUnchangedFlags(cpu.Flag, cpuCpy.Flag);
}

void LoadRegisterTest::TestLoadRegisterEffectsZeroFlag(m6502::Byte opcode) {
  using namespace m6502;
  CPU cpuCpy;
  s32 expectedCycles = 3;
  Byte expectedValue = 0;

  // setup memory for test
  mem[0xFFFC] = opcode;
  mem[0xFFFD] = expectedValue;

  cpuCpy = cpu;

  // when:
  cpu.Execute(expectedCycles, mem);

  // then:
  REQUIRE(cpu.Flag.N == 0);
  REQUIRE(cpu.Flag.Z == 1);
  ValidateUnchangedFlags(cpu.Flag, cpuCpy.Flag);
}

TEST_CASE_METHOD(LoadRegisterTest, "Load Register A") {
  using namespace m6502;

  cpu.Reset(0xFFFC, mem);

  SECTION("LDA Absolute can load a value into the A register") {
    TestLoadRegisterAbsolute(CPU::INS_LDA_ABS, &CPU::A);
  }

  SECTION("LDA Absolute X can load a value into the A register") {
    TestLoadRegisterAbsoluteX(CPU::INS_LDA_ABSX, &CPU::A);
  }

  SECTION("LDA Absolute X can load a value into the A register when a page boundary is crossed") {
    TestLoadRegisterAbsoluteXWhenCrossingPage(CPU::INS_LDA_ABSX, &CPU::A);
  }

  SECTION("LDA Absolute Y can load a value into the A register") {
    TestLoadRegisterAbsoluteY(CPU::INS_LDA_ABSY, &CPU::A);
  }

  SECTION("LDA Absolute Y can load a value into the A register when a page boundary is crossed") {
    TestLoadRegisterAbsoluteYWhenCrossingPage(CPU::INS_LDA_ABSY, &CPU::A);
  }

  SECTION("LDA Immediate can load a value into the A register") {
    TestLoadRegisterImmediate(CPU::INS_LDA_IM, &CPU::A);
  }

  SECTION("LDA Indirect X can load a value into the A register") {
    TestLoadRegisterIndirectX(CPU::INS_LDA_INDX, &CPU::A);
  }

  SECTION("LDA Indirect Y can load a value into the A register") {
    TestLoadRegisterIndirectY(CPU::INS_LDA_INDY, &CPU::A);
  }

  SECTION("LDA Zero Page can load a value into the A register") {
    TestLoadRegisterZeroPage(CPU::INS_LDA_ZP, &CPU::A);
  }

  SECTION("LDA Zero Page X can load a value into the A register") {
    TestLoadRegisterZeroPageX(CPU::INS_LDA_ZPX, &CPU::A);
  }

  SECTION("LDA can effect the negative flag") {
    TestLoadRegisterEffectsNegativeFlag(CPU::INS_LDA_IM);
  }

  SECTION("LDA can effect the zero flag") {
    TestLoadRegisterEffectsZeroFlag(CPU::INS_LDA_IM);
  }
}

TEST_CASE_METHOD(LoadRegisterTest, "Load Register X") {
  using namespace m6502;

  cpu.Reset(0xFFFC, mem);

  SECTION("LDX Absolute can load a value into the X register") {
    TestLoadRegisterAbsolute(CPU::INS_LDX_ABS, &CPU::X);
  }

  SECTION("LDX Absolute Y can load a value into the X register") {
    TestLoadRegisterAbsoluteY(CPU::INS_LDX_ABSY, &CPU::X);
  }

  SECTION("LDX Absolute Y can load a value into the X register when a page boundary is crossed") {
    TestLoadRegisterAbsoluteYWhenCrossingPage(CPU::INS_LDX_ABSY, &CPU::X);
  }

  SECTION("LDX Immediate can load a value into the X register") {
    TestLoadRegisterImmediate(CPU::INS_LDX_IM, &CPU::X);
  }

  SECTION("LDX Zero Page can load a value into the X register") {
    TestLoadRegisterZeroPage(CPU::INS_LDX_ZP, &CPU::X);
  }

  SECTION("LDX Zero Page Y can load a value into the X register") {
    TestLoadRegisterZeroPageY(CPU::INS_LDX_ZPY, &CPU::X);
  }

  SECTION("LDX can effect the negative flag") {
    TestLoadRegisterEffectsNegativeFlag(CPU::INS_LDX_IM);
  }

  SECTION("LDX can effect the zero flag") {
    TestLoadRegisterEffectsZeroFlag(CPU::INS_LDX_IM);
  }
}

TEST_CASE_METHOD(LoadRegisterTest, "Load Register Y") {
  using namespace m6502;

  cpu.Reset(0xFFFC, mem);

  SECTION("LDY Absolute can load a value into the Y register") {
    TestLoadRegisterAbsolute(CPU::INS_LDY_ABS, &CPU::Y);
  }

  SECTION("LDY Absolute X can load a value into the Y register") {
    TestLoadRegisterAbsoluteX(CPU::INS_LDY_ABSX, &CPU::Y);
  }

  SECTION("LDY Absolute X can load a value into the Y register when a page boundary is crossed") {
    TestLoadRegisterAbsoluteXWhenCrossingPage(CPU::INS_LDY_ABSX, &CPU::Y);
  }

  SECTION("LDY Immediate can load a value into the Y register") {
    TestLoadRegisterImmediate(CPU::INS_LDY_IM, &CPU::Y);
  }

  SECTION("LDY Zero Page can load a value into the Y register") {
    TestLoadRegisterZeroPage(CPU::INS_LDY_ZP, &CPU::Y);
  }

  SECTION("LDY Zero Page X can load a value into the Y register") {
    TestLoadRegisterZeroPageX(CPU::INS_LDY_ZPX, &CPU::Y);
  }

  SECTION("LDY can effect the negative flag") {
    TestLoadRegisterEffectsNegativeFlag(CPU::INS_LDY_IM);
  }

  SECTION("LDY can effect the zero flag") {
    TestLoadRegisterEffectsZeroFlag(CPU::INS_LDY_IM);
  }
}