#include <catch2/catch_test_macros.hpp>
#include "m6502.h"

TEST_CASE("CPU Reset tests") {
  using namespace m6502;
  CPU cpu;
  Memory mem;

  SECTION("Reset sets all bytes to 0") {
    cpu.Reset(mem);

    Byte hasAnyNonZeroBytes = 0;

    // loop over all entries in mem and carry out a logical OR
    // if any non-zero values are present hasAnyNonZeroBytes
    // will no longer be 0
    for (u32 i = 0; i < Memory::MAX_MEM; i++) {
      hasAnyNonZeroBytes |= mem[i];
    }

    REQUIRE(hasAnyNonZeroBytes == 0);
  }

  SECTION("Reset sets program counter to reset vector") {
    Word expectedVector = 0xFBED;
    cpu.Reset(expectedVector, mem);

    REQUIRE(expectedVector == cpu.PC);
  }

  SECTION("Reset sets program counter to 0xFFFC if no reset vector provided") {
    Word expectedVector = 0xFFFC;
    cpu.Reset(mem);

    REQUIRE(expectedVector == cpu.PC);
  }
}

TEST_CASE("Base execution tests") {
  using namespace m6502;
  CPU cpu;
  Memory mem;

  cpu.Reset(0xFFFC, mem);

  SECTION("Executes no cycles when 0 cycles passed") {
    s32 expectedCycles = 0;

    s32 actualCycles = cpu.Execute(expectedCycles, mem);

    REQUIRE(expectedCycles == actualCycles);
  }

  SECTION("CPU will execute more cycles than requested if needed for an instruction") {
    s32 expectedCycles = 2;

    // setup memory for test
    mem[0xFFFC] = CPU::INS_LDA_IM;
    mem[0xFFFD] = 0x37;

    s32 actualCycles = cpu.Execute(1, mem);

    REQUIRE(expectedCycles == actualCycles);
  }
}