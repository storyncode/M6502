#include <catch2/catch_test_macros.hpp>
#include "m6502.h"

class TransferTest {
protected:
  m6502::CPU cpu;
  m6502::Memory mem;
public:
  void TransferRegister(m6502::Byte opcode, m6502::Byte m6502::CPU::*source, m6502::Byte m6502::CPU::*target);
};

void TransferTest::TransferRegister(m6502::Byte opcode, m6502::Byte m6502::CPU::*source, m6502::Byte m6502::CPU::*target) {
  using namespace m6502;
  s32 expectedCycles = 2;
  Byte expectedValue = 0xA9;

  cpu.*source = expectedValue;
  cpu.*target = 0x32;

  mem[0xFFFC] = opcode;

  s32 actualCycles = cpu.Execute(expectedCycles, mem);
  Byte actualValue = cpu.*target;

  REQUIRE(expectedCycles == actualCycles);
  REQUIRE(expectedValue == actualValue);
}

TEST_CASE_METHOD(TransferTest, "Transfer to A Register") {
  using namespace m6502;

  cpu.Reset(0xFFFC, mem);

  SECTION("Transfer X Register to A Register sets the A register to the expected value") {
    TransferRegister(CPU::INS_TXA, &CPU::X, &CPU::A);
  }

  SECTION("Transfer Y Register to A Register sets A register to the expected value") {
    TransferRegister(CPU::INS_TYA, &CPU::Y, &CPU::A);
  }
}

TEST_CASE_METHOD(TransferTest, "Transfer to X Register") {
  using namespace m6502;

  cpu.Reset(0xFFFC, mem);

  SECTION("Transfer A Register to X Register sets the X register to the expected value") {
    TransferRegister(CPU::INS_TAX, &CPU::A, &CPU::X);
  }

  SECTION("Transfer Stack Pointer to X Register sets X register to the expected value") {
    TransferRegister(CPU::INS_TSX, &CPU::SP, &CPU::X);
  }
}

TEST_CASE_METHOD(TransferTest, "Transfer to Y Register") {
  using namespace m6502;

  cpu.Reset(0xFFFC, mem);

  SECTION("Transfer A Register to Y Register sets the Y register to the expected value") {
    TransferRegister(CPU::INS_TAY, &CPU::A, &CPU::Y);
  }
}

TEST_CASE_METHOD(TransferTest, "Transfer to Stack Pointer") {
  using namespace m6502;

  cpu.Reset(0xFFFC, mem);

  SECTION("Transfer X Register to Stack Pointer sets the Stack Pointer to the expected value") {
    TransferRegister(CPU::INS_TXS, &CPU::X, &CPU::SP);
  }
}