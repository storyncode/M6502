#include <catch2/catch_test_macros.hpp>
#include "m6502.h"

TEST_CASE("Push to Stack") {
  using namespace m6502;
  CPU cpu;
  Memory mem;

  cpu.Reset(mem);

  SECTION("Push A Register to Stack should set the current stack location to the value in the A register") {
    s32 expectedCycles = 3;
    Byte expectedValue = 0x7A;

    cpu.SP = 0x4A;
    cpu.A = expectedValue;

    mem[0xFFFC] = CPU::INS_PHA;
    mem[0x014A] = 0xD1; // ensure expected location does not have expected value prior to execution

    s32 actualCycles = cpu.Execute(expectedCycles, mem);
    Byte actualValue = mem[0x14A];

    REQUIRE(expectedCycles == actualCycles);
    REQUIRE(expectedValue == actualValue);
  }

  SECTION("Push Processor Status to Stack should set the current stack location to the value in the Processor Status with Break and Unused flags set") {
    s32 expectedCycles = 3;
    Byte processorState = 0x3A;
    Byte expectedValue = processorState | CPU::BreakFlagBit | CPU::UnusedFlagBit;

    cpu.SP = 0x4A;
    cpu.PS = processorState;

    mem[0xFFFC] = CPU::INS_PHP;
    mem[0x014A] = 0xD1; // ensure expected location does not have expected value prior to execution

    s32 actualCycles = cpu.Execute(expectedCycles, mem);
    Byte actualValue = mem[0x14A];

    REQUIRE(expectedCycles == actualCycles);
    REQUIRE(expectedValue == actualValue);
  }

  SECTION("Push Processor Status to Stack should always set the Break flag and Unused flag") {
    s32 expectedCycles = 3;
    Byte processorState = 0;
    Byte expectedValue = processorState | CPU::BreakFlagBit | CPU::UnusedFlagBit;

    cpu.SP = 0x4A;
    cpu.PS = processorState;

    mem[0xFFFC] = CPU::INS_PHP;
    mem[0x014A] = 0xD1; // ensure expected location does not have expected value prior to execution

    s32 actualCycles = cpu.Execute(expectedCycles, mem);
    Byte actualValue = mem[0x14A];

    REQUIRE(expectedCycles == actualCycles);
    REQUIRE(expectedValue == actualValue);
  }
}

TEST_CASE("Pull from Stack") {
  using namespace m6502;
  CPU cpu;
  Memory mem;

  cpu.Reset(mem);

  SECTION("Pull Accumulator from Stack should set A register to value from stack") {
    s32 expectedCycles = 4;
    Byte expectedValue = 0x9F;

    cpu.SP = 0x4A;
    cpu.A = 0;

    mem[0xFFFC] = CPU::INS_PLA;
    mem[0x014B] = expectedValue; // SP incremented prior to popping value

    s32 actualCycles = cpu.Execute(expectedCycles, mem);
    Byte actualValue = cpu.A;

    REQUIRE(expectedCycles == actualCycles);
    REQUIRE(expectedValue == actualValue);
  }

  SECTION("Pull Processor State from Stack should set Processor State to value on Stack excluding Break and Unused flags") {
    s32 expectedCycles = 4;
    Byte processorState = 0xA9;
    // should exclude the values of the Break and Unused flags
    Byte expectedValue = processorState & (~(CPU::BreakFlagBit | CPU::UnusedFlagBit));

    cpu.SP = 0x4A;
    cpu.PS = 0;

    mem[0xFFFC] = CPU::INS_PLP;
    mem[0x014B] = processorState; // ensure expected location does not have expected value prior to execution

    s32 actualCycles = cpu.Execute(expectedCycles, mem);
    Byte actualValue = cpu.PS;

    REQUIRE(expectedCycles == actualCycles);
    REQUIRE(expectedValue == actualValue);
  }

  SECTION("Pull Processor State from Stack should always have Break and Unused flags unset (0)") {
    s32 expectedCycles = 4;
    Byte allBitsSet = 0xFF;
    // should exclude the values of the Break and Unused flags
    Byte expectedValue = allBitsSet & (~(CPU::BreakFlagBit | CPU::UnusedFlagBit)); // should be 0xCF

    cpu.SP = 0x4A;
    cpu.PS = 0;

    mem[0xFFFC] = CPU::INS_PLP;
    mem[0x014B] = allBitsSet; // ensure expected location does not have expected value prior to execution

    s32 actualCycles = cpu.Execute(expectedCycles, mem);
    Byte actualValue = cpu.PS;

    REQUIRE(expectedCycles == actualCycles);
    REQUIRE(expectedValue == actualValue);
  }
}