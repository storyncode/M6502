#include "m6502.h"

#define ASSERT( Condition, Text ) { if ( !Condition ) { throw -1; } }

m6502::Word m6502::CPU::AddrAbsolute(s32 &cycles, Memory &memory) {
  Word address = FetchWord(cycles, memory);

  return address;
}

m6502::Word m6502::CPU::AddrAbsoluteX(s32 &cycles, Memory &memory) {
  Word address = FetchWord(cycles, memory);
  Word addressX = address + X;

  // if the page boundary was crossed, address XOR addressX
  // will result in 0x0010
  const bool PageBoundaryCrossed = (address ^ addressX) >> 8;
  if (PageBoundaryCrossed) {
    cycles--;
  }

  return addressX;
}

m6502::Word m6502::CPU::AddrAbsoluteY(s32 &cycles, Memory &memory) {
  Word address = FetchWord(cycles, memory);
  Word addressY = address + Y;

  // if the page boundary was crossed, address XOR addressY
  // will result in 0x0010
  const bool PageBoundaryCrossed = (address ^ addressY) >> 8;
  if (PageBoundaryCrossed) {
    cycles--;
  }

  return addressY;
}

m6502::Word m6502::CPU::AddrIndirectX(s32 &cycles, Memory &memory) {
  Byte address = FetchByte(cycles, memory);
  address += X;
  cycles--;

  Word effectiveAddress = ReadWord(cycles, memory, address);

  return effectiveAddress;
}

m6502::Word m6502::CPU::AddrIndirectY(s32 &cycles, Memory &memory) {
  Byte address = FetchByte(cycles, memory);
  Word effectiveAddress = ReadWord(cycles, memory, address);
  Word effectiveAddressY = effectiveAddress + Y;

  const bool PageBoundaryCrossed = (effectiveAddress ^ effectiveAddressY) >> 8;
  if (PageBoundaryCrossed) {
    cycles--;
  }

  return effectiveAddressY;
}

m6502::Word m6502::CPU::AddrIndirectY_NoPageBoundary(s32 &cycles, Memory &memory) {
  Byte address = FetchByte(cycles, memory);
  Word effectiveAddress = ReadWord(cycles, memory, address);
  Word effectiveAddressY = effectiveAddress + Y;
  cycles--;

  return effectiveAddressY;
}

m6502::Word m6502::CPU::AddrZeroPage(s32 &cycles, Memory &memory) {
  Byte address = FetchByte(cycles, memory);

  return (Word) address;
}

m6502::Word m6502::CPU::AddrZeroPageX(s32 &cycles, Memory &memory) {
  Byte address = FetchByte(cycles, memory);

  address += X;
  cycles--;

  return (Word) address;
}

m6502::Word m6502::CPU::AddrZeroPageY(s32 &cycles, Memory &memory) {
  Byte address = FetchByte(cycles, memory);

  address += Y;
  cycles--;

  return (Word) address;
}

m6502::s32 m6502::CPU::Execute(s32 cycles, Memory &memory) {
  // define lambdas
  auto LoadRegisterFromAddress = [&cycles, &memory, this](Word address, Byte &target) {
    target = ReadByte(cycles, memory, address);
    SetZeroAndNegativeFlags(target);
  };

  auto StoreRegisterToAddress = [&cycles, &memory, this](Word address, Byte value) {
    WriteByte(cycles, memory, address, value);
  };

  auto And = [&cycles, &memory, this](Word address) {
    Byte value = ReadByte(cycles, memory, address);
    A &= value;
    SetZeroAndNegativeFlags(A);
  };

  auto Bit = [this](Byte value) {
    Flag.Z = (A & value) == 0;
    Flag.N = (value & CPU::NegativeFlagBit) != 0;
    Flag.V = (value & CPU::OverflowFlagBit) != 0;
  };

  auto Eor = [&cycles, &memory, this](Word address) {
    Byte value = ReadByte(cycles, memory, address);
    A ^= value;
    SetZeroAndNegativeFlags(A);
  };

  auto Ora = [&cycles, &memory, this](Word address) {
    Byte value = ReadByte(cycles, memory, address);
    A |= value;
    SetZeroAndNegativeFlags(A);
  };

  s32 initialCycles = cycles;

  while (cycles > 0) {
    Byte instruction = FetchByte(cycles, memory);
    switch(instruction) {
      case INS_LDA_IM:
      {
        A = FetchByte(cycles, memory);
        SetZeroAndNegativeFlags(A);
      } break;
      case INS_LDA_ZP:
      {
        Word address = AddrZeroPage(cycles, memory);

        LoadRegisterFromAddress(address, A);
      } break;
      case INS_LDA_ZPX:
      {
        Word address = AddrZeroPageX(cycles, memory);

        LoadRegisterFromAddress(address, A);
      } break;
      case INS_LDA_ABS:
      {
        Word address = AddrAbsolute(cycles, memory);

        LoadRegisterFromAddress(address, A);
      } break;
      case INS_LDA_ABSX:
      {
        Word address = AddrAbsoluteX(cycles, memory);

        LoadRegisterFromAddress(address, A);
      } break;
      case INS_LDA_ABSY:
      {
        Word address = AddrAbsoluteY(cycles, memory);

        LoadRegisterFromAddress(address, A);
      } break;
      case INS_LDA_INDX:
      {
        Word address = AddrIndirectX(cycles, memory);

        LoadRegisterFromAddress(address, A);
      } break;
      case INS_LDA_INDY:
      {
        Word address = AddrIndirectY(cycles, memory);

        LoadRegisterFromAddress(address, A);
      } break;
      case INS_LDX_IM:
      {
        X = FetchByte(cycles, memory);
        SetZeroAndNegativeFlags(X);
      } break;
      case INS_LDX_ZP:
      {
        Word address = AddrZeroPage(cycles, memory);

        LoadRegisterFromAddress(address, X);
      } break;
      case INS_LDX_ZPY:
      {
        Word address = AddrZeroPageY(cycles, memory);

        LoadRegisterFromAddress(address, X);
      } break;
      case INS_LDX_ABS:
      {
        Word address = AddrAbsolute(cycles, memory);

        LoadRegisterFromAddress(address, X);
      } break;
      case INS_LDX_ABSY:
      {
        Word address = AddrAbsoluteY(cycles, memory);

        LoadRegisterFromAddress(address, X);
      } break;
      case INS_LDY_IM:
      {
        Y = FetchByte(cycles, memory);
        SetZeroAndNegativeFlags(Y);
      } break;
      case INS_LDY_ZP:
      {
        Word address = AddrZeroPage(cycles, memory);

        LoadRegisterFromAddress(address, Y);
      } break;
      case INS_LDY_ZPX:
      {
        Word address = AddrZeroPageX(cycles, memory);

        LoadRegisterFromAddress(address, Y);
      } break;
      case INS_LDY_ABS:
      {
        Word address = AddrAbsolute(cycles, memory);

        LoadRegisterFromAddress(address, Y);
      } break;
      case INS_LDY_ABSX:
      {
        Word address = AddrAbsoluteX(cycles, memory);

        LoadRegisterFromAddress(address, Y);
      } break;
      case INS_STA_ABS:
      {
        Word address = AddrAbsolute(cycles, memory);

        StoreRegisterToAddress(address, A);
      } break;
      case INS_STA_ABSX:
      {
        Word address = AddrAbsoluteX(cycles, memory);

        StoreRegisterToAddress(address, A);
      } break;
      case INS_STA_ABSY:
      {
        Word address = AddrAbsoluteY(cycles, memory);

        StoreRegisterToAddress(address, A);
      } break;
      case INS_STA_INDX:
      {
        Word address = AddrIndirectX(cycles, memory);

        StoreRegisterToAddress(address, A);
      } break;
      case INS_STA_INDY:
      {
        Word address = AddrIndirectY_NoPageBoundary(cycles, memory);

        StoreRegisterToAddress(address, A);
      } break;
      case INS_STA_ZP:
      {
        Word address = AddrZeroPage(cycles, memory);

        StoreRegisterToAddress(address, A);
      } break;
      case INS_STA_ZPX:
      {
        Word address = AddrZeroPageX(cycles, memory);

        StoreRegisterToAddress(address, A);
      } break;
      case INS_STX_ABS:
      {
        Word address = AddrAbsolute(cycles, memory);

        StoreRegisterToAddress(address, X);
      } break;
      case INS_STX_ZP:
      {
        Word address = AddrZeroPage(cycles, memory);

        StoreRegisterToAddress(address, X);
      } break;
      case INS_STX_ZPY:
      {
        Word address = AddrZeroPageY(cycles, memory);

        StoreRegisterToAddress(address, X);
      } break;
      case INS_STY_ABS:
      {
        Word address = AddrAbsolute(cycles, memory);

        StoreRegisterToAddress(address, Y);
      } break;
      case INS_STY_ZP:
      {
        Word address = AddrZeroPage(cycles, memory);

        StoreRegisterToAddress(address, Y);
      } break;
      case INS_STY_ZPX:
      {
        Word address = AddrZeroPageX(cycles, memory);

        StoreRegisterToAddress(address, Y);
      } break;
      case INS_TAX:
      {
        X = A;
        cycles--;
        SetZeroAndNegativeFlags(X);
      } break;
      case INS_TAY:
      {
        Y = A;
        cycles--;
        SetZeroAndNegativeFlags(Y);
      } break;
      case INS_TXA:
      {
        A = X;
        cycles--;
        SetZeroAndNegativeFlags(A);
      } break;
      case INS_TYA:
      {
        A = Y;
        cycles--;
        SetZeroAndNegativeFlags(A);
      } break;
      case INS_TSX:
      {
        X = SP;
        cycles--;
        SetZeroAndNegativeFlags(X);
      } break;
      case INS_TXS:
      {
        SP = X;
        cycles--;
      } break;
      case INS_PHA:
      {
        PushByteToStack(cycles, memory, A);
      } break;
      case INS_PHP:
      {
        // When pushing the processor state to stack
        // break flag and unused flag are always set
        // [               idk why              ]
        // [             ¯\_(ツ)_/¯             ]
        Byte value = PS | BreakFlagBit | UnusedFlagBit;
        PushByteToStack(cycles, memory, value);
      } break;
      case INS_PLA:
      {
        A = PopByteFromStack(cycles, memory);
        SetZeroAndNegativeFlags(A);
        cycles--;
      } break;
      case INS_PLP:
      {
        PS = PopByteFromStack(cycles, memory);

        // when popping processor state
        // from stack, break flag and unused flag
        // are set to false after popping
        // this is a result of the physical chip
        // we are emulating not having these pins
        // [               maybe?               ]
        // [             ¯\_(ツ)_/¯             ]
        Flag.B = Flag.Unused = 0;
        cycles--;
      } break;
      case INS_AND_ABS:
      {
        Word address = AddrAbsolute(cycles, memory);
        And(address);
      } break;
      case INS_AND_ABSX:
      {
        Word address = AddrAbsoluteX(cycles, memory);
        And(address);
      } break;
      case INS_AND_ABSY:
      {
        Word address = AddrAbsoluteY(cycles, memory);
        And(address);
      } break;
      case INS_AND_IM:
      {
        A &= FetchByte(cycles, memory);
        SetZeroAndNegativeFlags(A);
      } break;
      case INS_AND_INDX:
      {
        Word address = AddrIndirectX(cycles, memory);
        And(address);
      } break;
      case INS_AND_INDY:
      {
        Word address = AddrIndirectY(cycles, memory);
        And(address);
      } break;
      case INS_AND_ZP:
      {
        Word address = AddrZeroPage(cycles, memory);
        And(address);
      } break;
      case INS_AND_ZPX:
      {
        Word address = AddrZeroPageX(cycles, memory);
        And(address);
      } break;
      case INS_BIT_ABS:
      {
        Word address = AddrAbsolute(cycles, memory);
        Byte value = ReadByte(cycles, memory, address);
        Bit(value);
      } break;
      case INS_BIT_ZP:
      {
        Word address = AddrZeroPage(cycles, memory);
        Byte value = ReadByte(cycles, memory, address);
        Bit(value);
      } break;
      case INS_EOR_ABS:
      {
        Word address = AddrAbsolute(cycles, memory);
        Eor(address);
      } break;
      case INS_EOR_ABSX:
      {
        Word address = AddrAbsoluteX(cycles, memory);
        Eor(address);
      } break;
      case INS_EOR_ABSY:
      {
        Word address = AddrAbsoluteY(cycles, memory);
        Eor(address);
      } break;
      case INS_EOR_IM:
      {
        A ^= FetchByte(cycles, memory);
        SetZeroAndNegativeFlags(A);
      } break;
      case INS_EOR_INDX:
      {
        Word address = AddrIndirectX(cycles, memory);
        Eor(address);
      } break;
      case INS_EOR_INDY:
      {
        Word address = AddrIndirectY(cycles, memory);
        Eor(address);
      } break;
      case INS_EOR_ZP:
      {
        Word address = AddrZeroPage(cycles, memory);
        Eor(address);
      } break;
      case INS_EOR_ZPX:
      {
        Word address = AddrZeroPageX(cycles, memory);
        Eor(address);
      } break;
      case INS_ORA_ABS:
      {
        Word address = AddrAbsolute(cycles, memory);
        Ora(address);
      } break;
      case INS_ORA_ABSX:
      {
        Word address = AddrAbsoluteX(cycles, memory);
        Ora(address);
      } break;
      case INS_ORA_ABSY:
      {
        Word address = AddrAbsoluteY(cycles, memory);
        Ora(address);
      } break;
      case INS_ORA_IM:
      {
        A |= FetchByte(cycles, memory);
        SetZeroAndNegativeFlags(A);
      } break;
      case INS_ORA_INDX:
      {
        Word address = AddrIndirectX(cycles, memory);
        Ora(address);
      } break;
      case INS_ORA_INDY:
      {
        Word address = AddrIndirectY(cycles, memory);
        Ora(address);
      } break;
      case INS_ORA_ZP:
      {
        Word address = AddrZeroPage(cycles, memory);
        Ora(address);
      } break;
      case INS_ORA_ZPX:
      {
        Word address = AddrZeroPageX(cycles, memory);
        Ora(address);
      } break;
      default:
      {
        printf("Invalid instruction %d", instruction);
        cycles--;
      }
    }
  }

  // calculate the actual number of cycles used
  return initialCycles - cycles;
}