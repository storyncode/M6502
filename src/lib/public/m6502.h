#pragma once
#include <stdio.h>
#include <stdlib.h>

// http://www.obelisk.me.uk/6502/

namespace m6502
{
  using SByte = char;
  using Byte = unsigned char;
  using Word = unsigned short;

  using u32 = unsigned int;
  using s32 = signed int;

  struct Memory;
  struct CPU;
  struct StatusFlags;
}

struct m6502::Memory
{
  static constexpr u32 MAX_MEM = 1024 * 64;
  Byte Data[MAX_MEM];

  void Initialise()
  {
    for ( u32 i = 0; i < MAX_MEM; i++ )
    {
      Data[i] = 0;
    }
  }

  /** read 1 byte */
  Byte operator[]( u32 Address ) const
  {
    return Data[Address];
  }

  /** write 1 byte */
  Byte& operator[]( u32 Address )
  {
    return Data[Address];
  }
};

struct m6502::StatusFlags
{
  Byte C : 1;	//0: Carry Flag
  Byte Z : 1;	//1: Zero Flag
  Byte I : 1; //2: Interrupt disable
  Byte D : 1; //3: Decimal mode
  Byte B : 1; //4: Break
  Byte Unused : 1; //5: Unused
  Byte V : 1; //6: Overflow
  Byte N : 1; //7: Negative
};

struct m6502::CPU
{
  Word PC;		//program counter
  Byte SP;		//stack pointer

  Byte A, X, Y;	//registers

  union // processor status
  {
    Byte PS;
    StatusFlags Flag;
  };

  /* Setup */

  void Reset(Memory& memory )
  {
    Reset( 0xFFFC, memory );
  }

  void Reset(Word resetVector, Memory& memory )
  {
    PC = resetVector;
    SP = 0xFF;
    Flag.C = Flag.Z = Flag.I = Flag.D = Flag.B = Flag.V = Flag.N = 0;
    A = X = Y = 0;
    memory.Initialise();
  }

  /* Addressing Modes */
  Word AddrAbsolute(s32 &cycles, Memory &memory);
  Word AddrAbsoluteX(s32 &cycles, Memory &memory);
  Word AddrAbsoluteY(s32 &cycles, Memory &memory);
  Word AddrIndirectX(s32 &cycles, Memory &memory);
  Word AddrIndirectY(s32 &cycles, Memory &memory);
  Word AddrIndirectY_NoPageBoundary(s32 &cycles, Memory &memory);
  Word AddrZeroPage(s32 &cycles, Memory &memory);
  Word AddrZeroPageX(s32 &cycles, Memory &memory);
  Word AddrZeroPageY(s32 &cycles, Memory &memory);

  /* Memory operations */

  Byte FetchByte(s32 &cycles, Memory &memory) {
    Byte value = ReadByte(cycles, memory, PC);
    PC++;
    return value;
  }

  Byte ReadByte(s32 &cycles, Memory &memory, Word address) {
    Byte value = memory[address];
    cycles--;
    return value;
  }

  void WriteByte(s32 &cycles, Memory &memory, Word address, Byte value) {
    memory[address] = value;
    cycles--;
  }

  Word FetchWord(s32 &cycles, Memory &memory) {
    Byte lo = FetchByte(cycles, memory);
    Byte hi = FetchByte(cycles, memory);

    Word value = lo;
    value |= (hi << 8);

    return value;
  }

  Word ReadWord(s32 &cycles, Memory &memory, Word address) {
    Byte lo = ReadByte(cycles, memory, address);
    Byte hi = ReadByte(cycles, memory, (address + 1));

    Word value = lo;
    value |= (hi << 8);

    return value;
  }

  /* Stack Operations */
  Word StackPointerToAddress() const {
    return 0x100 | SP;
  }

  Byte PopByteFromStack(s32 &cycles, Memory &memory) {
    // Advance stack pointer and consume a cycle
    SP++;
    cycles--;

    // Convert SP to address and read value from memory
    Word address = StackPointerToAddress();
    Byte value = ReadByte(cycles, memory, address);

    return value;
  }

  void PushByteToStack(s32 &cycles, Memory &memory, Byte value) {
    Word address = StackPointerToAddress();
    WriteByte(cycles, memory, address, value);
    SP--;
    cycles--;
  }

  /* Flag Operations */
  void SetZeroAndNegativeFlags(Byte value) {
    Flag.N = (value & NegativeFlagBit) > 0;
    Flag.Z = (value == 0);
  }

  // Process status bits
  static constexpr Byte
      NegativeFlagBit = 0b10000000,
      OverflowFlagBit = 0b01000000,
      BreakFlagBit = 0b000010000,
      UnusedFlagBit = 0b000100000,
      InterruptDisableFlagBit = 0b000000100,
      ZeroBit = 0b00000001;

  static constexpr Byte
    // LDA:
    INS_LDA_ABS = 0xAD,
    INS_LDA_ABSX = 0xBD,
    INS_LDA_ABSY = 0xB9,
    INS_LDA_IM = 0xA9,
    INS_LDA_INDX = 0xA1,
    INS_LDA_INDY = 0xB1,
    INS_LDA_ZP = 0xA5,
    INS_LDA_ZPX = 0xB5,
    // LDX:
    INS_LDX_ABS = 0xAE,
    INS_LDX_ABSY = 0xBE,
    INS_LDX_IM = 0xA2,
    INS_LDX_ZP = 0xA6,
    INS_LDX_ZPY = 0xB6,
    // LDY:
    INS_LDY_ABS = 0xAC,
    INS_LDY_ABSX = 0xBC,
    INS_LDY_IM = 0xA0,
    INS_LDY_ZP = 0xA4,
    INS_LDY_ZPX = 0xB4,
    // STA:
    INS_STA_ZP = 0x85,
    INS_STA_ZPX = 0x95,
    INS_STA_ABS = 0x8D,
    INS_STA_ABSX = 0x9D,
    INS_STA_ABSY = 0x99,
    INS_STA_INDX = 0x81,
    INS_STA_INDY = 0x91,
    // STX:
    INS_STX_ZP = 0x86,
    INS_STX_ZPY = 0x96,
    INS_STX_ABS = 0x8E,
    // STY:
    INS_STY_ZP = 0x84,
    INS_STY_ZPX = 0x94,
    INS_STY_ABS = 0x8C,
    // Transfer register operations:
    INS_TAX = 0xAA,
    INS_TAY = 0xA8,
    INS_TXA = 0x8A,
    INS_TYA = 0x98,
    // Transfer stack pointer operations:
    INS_TSX = 0xBA,
    INS_TXS = 0x9A,
    INS_PHA = 0x48,
    INS_PHP = 0x08,
    INS_PLA = 0x68,
    INS_PLP = 0x28,
    // Logical operations
    // AND
    INS_AND_ABS = 0x2D,
    INS_AND_ABSX = 0x3D,
    INS_AND_ABSY = 0x39,
    INS_AND_IM = 0x29,
    INS_AND_INDX = 0x21,
    INS_AND_INDY = 0x31,
    INS_AND_ZP = 0x25,
    INS_AND_ZPX = 0x35,
    // BIT
    INS_BIT_ABS = 0x2C,
    INS_BIT_ZP = 0x24,
    // EOR
    INS_EOR_ABS = 0x4D,
    INS_EOR_ABSX = 0x5D,
    INS_EOR_ABSY = 0x59,
    INS_EOR_IM = 0x49,
    INS_EOR_INDX = 0x41,
    INS_EOR_INDY = 0x51,
    INS_EOR_ZP = 0x45,
    INS_EOR_ZPX = 0x55,
    // ORA
    INS_ORA_ABS = 0x0D,
    INS_ORA_ABSX = 0x1D,
    INS_ORA_ABSY = 0x19,
    INS_ORA_IM = 0x09,
    INS_ORA_INDX = 0x01,
    INS_ORA_INDY = 0x11,
    INS_ORA_ZP = 0x05,
    INS_ORA_ZPX = 0x15;

  s32 Execute(s32 address, Memory& memory);
};